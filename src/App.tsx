import * as React from 'react';
import './App.css';

const logo = require('./ra-logo.svg');

class App extends React.Component {
  render() {
    return (
      <div className="ra-nav">
        <div className="ra-nav__logo">
          <img src={logo} />
        </div>

        <ul className="ra-nav__menu ra-menu">
          <li className="ra-menu-item ra-menu-item--top3">
            <a href="#">
              <span className="ra-menu-item__icon" />
              <span className="ra-menu-item__text">Top 3 Report Card</span>
              <ul className="ra-menu-item__submenu">
                <li id="top3-welcome"><a href="#welcome">Welcome</a></li>
                <li id="top3-criticaltop3"><a href="#criticaltop3">Critical Top 3</a></li>
                <li id="top3-reportcard"><a href="#reportcard">Report Card</a></li>
              </ul>
            </a>
          </li>
          <li className="ra-menu-item ra-menu-item--facility-unit-comp">
            <a href="#">
              <span className="ra-menu-item__icon" />
              <span className="ra-menu-item__text">Facility Unit Comparison</span>
            </a>
          </li>
          <li className="ra-menu-item ra-menu-item--safeguard-ranking">
            <a href="#">
              <span className="ra-menu-item__icon" />
              <span className="ra-menu-item__text">Safeguard Ranking</span>
            </a>
          </li>
          <li className="ra-menu-item ra-menu-item--hazardous-scenario-viewer is-active">
            <a href="#">
              <span className="ra-menu-item__icon" />
              <span className="ra-menu-item__text">Hazardous Scenario Viewer</span>
            </a>
          </li>
        </ul>

        <ul className="ra-nav__share ra-menu">
          <li className="ra-menu-item ra-menu-item--share">
            <a href="#">
              <span className="ra-menu-item__icon" />
              <span className="ra-menu-item__text">Share</span>
            </a>
          </li>
        </ul>
      </div>
    );
  }
}

export default App;
